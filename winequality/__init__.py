import pandas as pd
import sklearn as sk
import numpy as np
import pymongo

from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import ComplementNB
from sklearn import svm

from sklearn import metrics

if __name__ == "__main__":

    client = pymongo.MongoClient("mongodb+srv://jaydouglas:mongoadmin87@cluster0-prk7h.mongodb.net/test?retryWrites=true&w=majority")

    db = client.classification
    collection = db.winequality
    cursor = collection.find({})

    print(cursor)

    #input_red = pd.read_csv("D:/datasets/wine-quality/winequality-red.csv", header=0,sep=",")
    input_red = pd.DataFrame(cursor)
    red_y = input_red['quality'].values

    red_features = list(input_red.columns[1:-1])

    red_x = input_red[red_features]

    print(red_x)

    #First 80% of data
    train_red_x = red_x[:(int)(-(0.2)*red_x.shape[0])]
    train_red_y = red_y[:(int)(-(0.2) * len(red_y))]

    #Last 20% of data
    test_red_x = red_x[(int)(0.8*red_x.shape[0]):]
    test_red_y = red_y[(int)(0.8* len(red_y)):] #inefficient if you never need the category as a dataframe

    #Extract a column from the dataframe as a series, and extract the unique labels
    #Some "classes" not present in training dataset, so should just generate it manually
    #print(red_y["quality"].unique())

    categories = [0,1,2,3,4,5,6,7,8,9,10]

    model = svm.SVC()

    model.fit(train_red_x, train_red_y)

    pred = model.predict(train_red_x)

    stats = train_red_y

    print(metrics.classification_report(stats, pred))
    print(metrics.confusion_matrix(stats, pred))


